<?php 

function my_set_image_meta_upon_image_upload( $post_ID ) {

    if ( wp_attachment_is_image( $post_ID ) ) {

        $my_image_title = get_post( $post_ID )->post_title;
        $my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );
        $my_image_title = ucwords( strtolower( $my_image_title ) );

        $my_image_meta = array(
            'ID'        => $post_ID,            
            'post_title'    => $my_image_title, 
        );

        update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );
        wp_update_post( $my_image_meta );

    } 
}

function alt_text_fetcher() {
    $images = get_children(array(
        'post_parent' => $post->ID,
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'numberposts' => 999
    ));
    
    $data = array();
    
    if ( $images ) { 
        foreach ( $images as $attachment_id => $attachment ) {
            $alt = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );
            $src = wp_get_attachment_image_url ($attachment->ID,'full');
            $postID = $attachment->ID;
            
            if($alt != '')
                array_push($data, array('alttext' => $alt, 'src' => $src, 'post_ID' => $postID));
        }
    }
    
    return json_encode($data);
}


add_action( 'add_attachment', 'set_meta_on_upload' );
add_action('get_header', 'alt_text_fetcher');

?>