<?php

include_once(plugin_dir_path( __FILE__ ) . 'fetcher-ajax.php' );

add_action( 'wp_enqueue_scripts', 'load_fetcher_scripts' );
function load_fetcher_scripts() {
	wp_enqueue_script('fetcher-ajax-js', '/wp-content/plugins/alt-text-monitoring/js/fetcher-ajax.js', array( 'jquery'));
	wp_localize_script( 'fetcher-ajax-js', 'fetcher_ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_enqueue_script('fetcher-js', '/wp-content/plugins/alt-text-monitoring/js/fetcher.js', array( 'jquery' ));
	wp_enqueue_style( 'fetcher-style', '/wp-content/plugins/alt-text-monitoring/css/style.css');
}



function load_fetcher(){
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		init_fetch();
	});

	jQuery(window).load(function(){
		init_monitoring();
		init_monitoring_func();
	});

	function init_fetch(){
		var data = <?php echo alt_text_fetcher() ?>;
		jQuery.each( data, function() {
			var alt = this['alttext'];
			var src = this['src'];
			var post_ID = this['post_ID'];
			update_alt_by_src(src, alt);
		});
	}
</script>
<?php
}

add_action('wp_footer', 'load_fetcher');


?>