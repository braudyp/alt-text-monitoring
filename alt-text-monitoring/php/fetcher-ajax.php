<?php 

function update_image_alt_text() {
	global $wpdb; 

	$alt = strval($_POST['alt']);
	$image_url = strval($_POST['src']);
	$postID = attachment_url_to_postid($image_url);
	
	update_post_meta( $postID, '_wp_attachment_image_alt', $alt );
	
	wp_die();
}

add_action( 'wp_ajax_update_image_alt_text', 'update_image_alt_text' );

?>