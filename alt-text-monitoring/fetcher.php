<?php
/*
 * Plugin Name: Alt Text Monitoring
 * Version: 1.0
 * Description: This plugins monitors the alt text of the images in your site. Also lets you update alt text via frontend.
 * Author: Braudy Pedrosa
 * Requires at least: 4.5
 * Tested up to: 4.8
 *
 *
 * @package WordPress
 * @author Braudy Pedrosa
 * @since 1.0.0
 */

include_once(plugin_dir_path( __FILE__ ) . '/php/fetcher-functions.php' );

include_once(plugin_dir_path( __FILE__ ) . '/php/automate.php' );


