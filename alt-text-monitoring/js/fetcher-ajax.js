
function wp_ajax_set_alt(alt, src) {
	jQuery.ajax({
		url: fetcher_ajax.ajaxurl,
		type: 'POST',
		data: {
			'action': 'update_image_alt_text',
			'alt': alt,
			'src': src
		},
		success: function(response) {
			console.log(response);
		},
	});
}
