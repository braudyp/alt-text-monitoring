	var img_alt_array = [];
	var count = 0;
    
	function update_alt_by_src(src, alt){
		var image = get_img_by_src(src);
		image.attr('alt',alt);
	}

	function get_img_by_src(src){
		return jQuery('img[src$="'+src+'"]');
	}

	function init_monitoring(){
		jQuery('body').prepend('<button class="monitor_alt_btn">All images have alt text</button>');
		
		
		jQuery('body img').each(function(){
			if(!jQuery(this).hasClass('avatar')) {
				if(jQuery(this).attr('alt') == ''){ 
					console.log(jQuery(this));
					jQuery('.monitor_alt_btn').addClass('error');
					jQuery('.monitor_alt_btn').html("Check alt text");
				}
			}
		});
			
		
	}

	function init_monitoring_func() {
		jQuery('.monitor_alt_btn').click(function(){
			get_images_without_alt();
		});
	}

	function setAlt(image_url) {
		var alt = prompt("Enter new alt text:");
		imageObject.attr('alt', alt);
		imageObject.css('border', 'none');
		wp_ajax_set_alt(alt, image_url);
	}

	function get_images_without_alt(){
		var images = [];
		jQuery('body img').each(function(){
			if(jQuery(this).attr('alt') == ''){
				var image_url = jQuery(this).attr('src');
						
				if(!jQuery(this).hasClass('avatar')) {
					jQuery(this).css('border', '3px solid red');
					images.push(jQuery(this));
				}
				
				jQuery(this).click(function(){
					setAlt(image_url);
				});
			}
		});
	
		return images;
	}